/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ucr.ac.cr.Modelo;

import java.awt.Rectangle;
import javax.swing.ImageIcon;

public class Enemigo_Bruja extends Enemigo {
    
    private ImageIcon imagenTeleport = new ImageIcon("./src/main/resources/Imagenes.Bruja/Bruja_Movimiento.gif");
    private int disparoX;
    private int disparoY;
    private long disparoTimer;
    private long disparoDeley;
    private long areaTimer;
    private long areaDeley;
    private int randomPosicion;
    private long moveTimer;
    private long moveDeley;
    private Sonido brujaTeleport;
    private Sonido brujaDisparo;
    private Sonido brujaArea;
    private Sonido piano;
    private boolean mitadSonidoEnemigo;

    public Enemigo_Bruja(ImageIcon imagen, int x, int y) {
        super(imagen, x, y);
        this.vidaMaxima = 64;
        this.vida = 64;
        this.areaColision = new Rectangle(x + 38, y + 19, 39, 93);
        this.movimiento = 0;
        this.disparoX = 4;
        this.disparoY = 4;
        this.disparoTimer = System.nanoTime();
        this.disparoDeley = 1500;
        this.areaTimer = System.nanoTime();
        this.areaDeley = 2500;
        this.moveTimer = System.nanoTime();
        this.moveDeley = 2000;
        this.imagenTeleport.getImage().flush();
    }

    public void tiempoDisparo() {
        long elapsed = (System.nanoTime() - disparoTimer) / 1000000;
        int randomX = (int) ((Math.random() * (3 - 1)) + 1);

        if (elapsed > disparoDeley) {
            brujaDisparo = new Sonido("./src/main/resources/Musica/BrujaDisparo.wav");
            brujaDisparo.iniciar();
            if (randomX == 1) {
                this.disparos.add(new Proyectil_Bruja(new ImageIcon("./src/main/resources/Imagenes.Bruja/Bruja_Proyectil.png"), x + 38, y + 70, 0, disparoY));
                this.disparos.add(new Proyectil_Bruja(new ImageIcon("./src/main/resources/Imagenes.Bruja/Bruja_Proyectil.png"), x + 38, y + 70, 0, -disparoY));
            }
            if (randomX == 2) {
                this.disparos.add(new Proyectil_Bruja(new ImageIcon("./src/main/resources/Imagenes.Bruja/Bruja_Proyectil.png"), x + 38, y + 70, disparoX, 0));
                this.disparos.add(new Proyectil_Bruja(new ImageIcon("./src/main/resources/Imagenes.Bruja/Bruja_Proyectil.png"), x + 38, y + 70, -disparoX, 0));
            }
            disparoTimer = System.nanoTime();
        }
    }

    public void tiempoZona() {
        long elapsed = (System.nanoTime() - areaTimer) / 1000000;

        int randomX = (int) ((Math.random() * (1187 - 30)) + 30);
        int randomY = (int) ((Math.random() * (675 - 192)) + 192);

        if (vida <= vidaMaxima / 2) {
            if (elapsed > areaDeley) {
                brujaArea = new Sonido("./src/main/resources/Musica/BrujaArea.wav");
                brujaArea.iniciar();
                areaTimer = System.nanoTime();
                ImageIcon area = new ImageIcon("./src/main/resources/Imagenes.Bruja/Bruja_Zona.gif");
                this.areas.add(new Proyectil_Bruja(area, randomX, randomY));
            }
        }
    }
    
    public void verificarMitadVida(double tiempo) {
        if (vida <= vidaMaxima / 2 && !mitadSonidoEnemigo) {
            piano =  new Sonido("./src/main/resources/Musica/Piano.wav");
            piano.setMicrosegundos(tiempo);
            piano.ciclo();
            mitadSonidoEnemigo = true;
        }
    }
    
    public Sonido getPiano() {
        return piano;
    }
    
    @Override
    public void movimiento(int x, int y) {
        int posicion;
        long elapsed = (System.nanoTime() - moveTimer) / 1000000;
        if (elapsed > moveDeley) {
            brujaTeleport = new Sonido("./src/main/resources/Musica/BrujaTeleport.wav");
            brujaTeleport.iniciar();
            posicion = (int) ((Math.random() * (5 - 1)) + 1);

            this.randomPosicion = posicion;

            if (randomPosicion == 1) {
                this.x = 586;
                this.y = 168;
            }
            if (randomPosicion == 2) {
                this.x = 586;
                this.y = 367;
            }
            if (randomPosicion == 3) {
                this.x = 74;
                this.y = 367;
            }
            if (randomPosicion == 4) {
                this.x = 1098;
                this.y = 367;
            }
            this.areaColision = new Rectangle(this.x + 38, this.y + 19, 39, 93);
            this.imagen = imagenTeleport;
            moveTimer = System.nanoTime();
        }
    }

    @Override
    public void movimientoDisparo() {
        for (int i = 0; i < disparos.size(); i++) {
            disparos.get(i).disparar();
        }
    }
}
