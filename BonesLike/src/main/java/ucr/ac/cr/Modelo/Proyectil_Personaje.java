/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ucr.ac.cr.Modelo;

import java.awt.Rectangle;
import javax.swing.ImageIcon;

public class Proyectil_Personaje extends Proyectil {

    private int direccion;

    public Proyectil_Personaje(ImageIcon imagen, int x, int y, int direccion) {
        super(imagen, x, y);
        this.direccion = direccion;
        this.movimientoX = 6;
        this.movimientoY = 6;
        this.areaColisionProyectil = new Rectangle(8, 8, 16, 16);
    }

    @Override
    public void disparar() {
        if (direccion == 0) {
            y = y - movimientoY;
        }
        if (direccion == 1) {
            y = y + movimientoY;
        }
        if (direccion == 2) {
            x = x - movimientoX;
        }
        if (direccion == 3) {
            x = x + movimientoX;
        }
    }

    @Override
    public void dispararLinea() {

    }
}
