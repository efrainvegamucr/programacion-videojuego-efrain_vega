/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ucr.ac.cr.Modelo;

import java.awt.Graphics;
import java.awt.Rectangle;
import javax.swing.ImageIcon;
import ucr.ac.cr.Vista.PanelJuego;

public abstract class Proyectil {

    protected ImageIcon imagen;
    protected int x;
    protected int y;
    protected int movimientoX;
    protected int movimientoY;
    protected Rectangle areaColisionProyectil;
    protected Rectangle areaColisionZona;

    public Proyectil(ImageIcon imagen, int x, int y) {
        this.imagen = imagen;
        this.x = x;
        this.y = y;
    }

    public ImageIcon getImagen() {
        return imagen;
    }

    public void setImagen(ImageIcon imagen) {
        this.imagen = imagen;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getMovimientoX() {
        return movimientoX;
    }

    public void setMovimientoX(int movimientoX) {
        this.movimientoX = movimientoX;
    }

    public int getMovimientoY() {
        return movimientoY;
    }

    public void setMovimientoY(int movimientoY) {
        this.movimientoY = movimientoY;
    }

    public Rectangle getAreaColisionProyectil() {
        return areaColisionProyectil;
    }

    public void setAreaColisionProyectil(Rectangle areaColisionProyectil) {
        this.areaColisionProyectil = areaColisionProyectil;
    }

    public Rectangle getAreaColisionZona() {
        return areaColisionZona;
    }

    public void setAreaColisionZona(Rectangle areaColisionZona) {
        this.areaColisionZona = areaColisionZona;
    }

    public abstract void disparar();

    public abstract void dispararLinea();

    public void paint(Graphics g, PanelJuego panelJuego) {
        imagen.paintIcon(panelJuego, g, x, y);
    }
}
