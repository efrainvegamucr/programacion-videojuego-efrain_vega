/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ucr.ac.cr.Modelo;

public class Puntuacion {

    private String nombre;
    private String puntuacion;

    public static final String[] ETIQUETAS_PUNTUACION = {"Nombre", "Puntuación"};

    public Puntuacion(String nombre, String puntuacion) {
        this.nombre = nombre;
        this.puntuacion = puntuacion;
    }

    public Puntuacion() {

    }

    public String setDatosPuntuacion(int indice) {
        switch (indice) {
            case 0:
                return this.getNombre();
            case 1:
                return this.getPuntuacion().toString();
        }
        return null;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPuntuacion() {
        return puntuacion;
    }

    public void setPuntuacion(String puntuacion) {
        this.puntuacion = puntuacion;
    }

    @Override
    public String toString() {
        return "Puntuacion{" + "nombre=" + nombre + ", puntuacion=" + puntuacion + '}';
    }
}
