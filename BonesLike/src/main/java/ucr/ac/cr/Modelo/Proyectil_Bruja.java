/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ucr.ac.cr.Modelo;

import java.awt.Rectangle;
import javax.swing.ImageIcon;

public class Proyectil_Bruja extends Proyectil {

    public Proyectil_Bruja(ImageIcon imagen, int x, int y, int movimientoX, int movimientoY) {
        super(imagen, x, y);
        this.movimientoX = movimientoX;
        this.movimientoY = movimientoY;
        this.areaColisionProyectil = new Rectangle(8, 8, 16, 16);
    }

    public Proyectil_Bruja(ImageIcon imagen, int x, int y) {
        super(imagen, x, y);
        this.areaColisionZona = new Rectangle(8, 8, 48, 48);
    }

    @Override
    public void disparar() {
        x = x - movimientoX;
        y = y - movimientoY;
    }

    @Override
    public void dispararLinea() {
    }
}
