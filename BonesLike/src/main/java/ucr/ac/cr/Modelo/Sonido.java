/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ucr.ac.cr.Modelo;

import java.io.File;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;

public class Sonido {

    private Clip clip;
    public File archivo;
    private FloatControl fc;
    private float volumenActual = 0;

    public Sonido(String directorio) {
        archivo = new File(directorio);
        try {
            AudioInputStream sonido = AudioSystem.getAudioInputStream(archivo);
            clip = AudioSystem.getClip();
            clip.open(sonido);
            fc = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
        } catch (Exception e) {
            System.out.println("No se encontro el archivo.");
        }
    }

    public double getMicrosegundos() {
        return clip.getMicrosecondPosition();
    }

    public void setMicrosegundos(double microsegundos) {
        clip.setMicrosecondPosition((long) microsegundos);
    }

    public void iniciar() {
        clip.start();
    }

    public void ciclo() {
        clip.loop(Clip.LOOP_CONTINUOUSLY);
    }

    public void detener() {
        clip.stop();
    }

    public void cerrar() {
        clip.close();
    }

    public void subirVolumen() {
        volumenActual += 1.0f;
        if (volumenActual > 0) {
            volumenActual = 0;
        }
        fc.setValue(volumenActual);
        System.out.println(fc.getValue());
    }

    public void bajarVolumen() {
        do {
            volumenActual -= 1.0f;
        } while (volumenActual != -70.0f);

        fc.setValue(volumenActual);
        System.out.println(fc.getValue());
    }
}
