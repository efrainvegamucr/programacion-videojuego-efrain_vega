/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ucr.ac.cr.Modelo;

import java.awt.Rectangle;
import javax.swing.ImageIcon;

public class Enemigo_Slime extends Enemigo {

    private int disparoX;
    private int disparoY;
    private long firingTimer;
    private long firingDeley;
    private long moveTimer;
    private long moveDeley;
    private Sonido slimeDisparo;
    private Sonido piano;
    private boolean mitadSonidoEnemigo;
        
    public Enemigo_Slime(ImageIcon imagen, int x, int y) {
        super(imagen, x, y);
        this.vidaMaxima = 64;
        this.vida = 64;
        this.areaColision = new Rectangle(x + 24, y + 51, 80, 69);
        this.movimiento = 2;
        this.disparoX = 3;
        this.disparoY = 3;
        this.firingTimer = System.nanoTime();
        this.firingDeley = 2500;
        this.moveTimer = System.nanoTime();
        this.moveDeley = 30;
        this.mitadSonidoEnemigo = false;
    }

    public void tiempoDisparo() {
        long elapsed = (System.nanoTime() - firingTimer) / 1000000;

        int randomX = (int) ((Math.random() * (3 - 1)) + 1);
        int randomY = (int) ((Math.random() * (3 - 1)) + 1);

        if (randomX == 2) {
            disparoX = -disparoX;
        }
        if (randomY == 2) {
            disparoY = -disparoY;
        }
        if (vida <= vidaMaxima / 2) {
            if (elapsed > firingDeley) {
                slimeDisparo = new Sonido("./src/main/resources/Musica/SlimeDisparo.wav");
                slimeDisparo.iniciar();
                firingTimer = System.nanoTime();
                this.disparos.add(new Proyectil_Slime(new ImageIcon("./src/main/resources/Imagenes.Slime/Slime_Proyectil.png"), x + 48, y + 87, disparoX, disparoY));
            }
        }
    }

    public void verificarMitadVida(double tiempo) {
        if (vida <= vidaMaxima / 2 && !mitadSonidoEnemigo) {
            piano =  new Sonido("./src/main/resources/Musica/Piano.wav");
            piano.setMicrosegundos(tiempo);
            piano.ciclo();
            mitadSonidoEnemigo = true;
        }
    }

    public Sonido getPiano() {
        return piano;
    }
    
    @Override
    public void movimiento(int x, int y) {
        int movimientoX = this.x - x + 32;
        int movimientoY = this.y - y + 36;

        long elapsed = (System.nanoTime() - moveTimer) / 1000000;
        if (elapsed > moveDeley) {
            this.areaColision = new Rectangle(this.x + 24, this.y + 51, 80, 69);
            if (movimientoX < 0) {
                this.x += this.movimiento;
            }
            if (movimientoX > 0) {
                this.x -= this.movimiento;
            }
            if (movimientoY < 0) {
                this.y += this.movimiento;
            }
            if (movimientoY > 0) {
                this.y -= this.movimiento;
            }
            this.imagen = new ImageIcon("./src/main/resources/Imagenes.Slime/Slime_Movimiento.gif");
            moveTimer = System.nanoTime();
        }
    }

    public void movimientoDisparo() {
        for (int i = 0; i < disparos.size(); i++) {
            disparos.get(i).disparar();
        }
    }

}
