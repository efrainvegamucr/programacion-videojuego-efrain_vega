/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ucr.ac.cr.Modelo;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import ucr.ac.cr.Vista.PanelJuego;

public abstract class Enemigo {

    protected ImageIcon imagen;
    protected int x;
    protected int y;
    protected int vidaMaxima;
    protected int vida;
    protected int movimiento;
    protected boolean estaVivo;
    protected Rectangle areaColision;
    protected ArrayList<Proyectil> disparos;
    protected ArrayList<Proyectil> disparosLinea;
    protected ArrayList<Proyectil> areas;

    public Enemigo(ImageIcon imagen, int x, int y) {
        this.imagen = imagen;
        this.x = x;
        this.y = y;
        this.estaVivo = true;
        this.disparos = new ArrayList<Proyectil>();
        this.disparosLinea = new ArrayList<Proyectil>();
        this.areas = new ArrayList<Proyectil>();
    }

    public ImageIcon getImagen() {
        return imagen;
    }

    public void setImagen(ImageIcon imagen) {
        this.imagen = imagen;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getVidaMaxima() {
        return vidaMaxima;
    }

    public void setVidaMaxima(int vidaMaxima) {
        this.vidaMaxima = vidaMaxima;
    }

    public int getVida() {
        return vida;
    }

    public void setVida(int vida) {
        this.vida = vida;
    }

    public boolean getEstaVivo() {
        return estaVivo;
    }

    public void setEstaVivo(boolean estaVivo) {
        this.estaVivo = estaVivo;
    }

    public Rectangle getAreaColision() {
        return areaColision;
    }

    public void setAreaColision(Rectangle areaColision) {
        this.areaColision = areaColision;
    }

    public ArrayList<Proyectil> getDisparos() {
        return disparos;
    }

    public void setDisparos(ArrayList<Proyectil> disparos) {
        this.disparos = disparos;
    }

    public ArrayList<Proyectil> getDisparosLinea() {
        return disparosLinea;
    }

    public void setDisparosLinea(ArrayList<Proyectil> disparosLinea) {
        this.disparosLinea = disparosLinea;
    }

    public ArrayList<Proyectil> getAreas() {
        return areas;
    }

    public void setAreas(ArrayList<Proyectil> areas) {
        this.areas = areas;
    }

    public abstract void movimiento(int x, int y);

    public abstract void movimientoDisparo();

    public void paintProyectiles(Graphics g, PanelJuego panelJuego) {
        for (int i = 0; i < disparos.size(); i++) {
            disparos.get(i).paint(g, panelJuego);
        }
    }

    public void paintProyectilesLinea(Graphics g, PanelJuego panelJuego) {
        for (int i = 0; i < disparosLinea.size(); i++) {
            disparosLinea.get(i).paint(g, panelJuego);
        }
    }

    public void paintAreas(Graphics g, PanelJuego panelJuego) {
        for (int i = 0; i < areas.size(); i++) {
            areas.get(i).paint(g, panelJuego);
        }
    }

    public void paint(Graphics g, PanelJuego panelJuego) {
        imagen.paintIcon(panelJuego, g, x, y);
    }
}
