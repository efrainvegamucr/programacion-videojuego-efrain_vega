/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ucr.ac.cr.Modelo;

import java.awt.Rectangle;
import javax.swing.ImageIcon;

public class Proyectil_Vampiro extends Proyectil {

    boolean numero;
    int limiteDerecho;
    int limiteIzquierdo;

    public Proyectil_Vampiro(ImageIcon imagen, int x, int y, int movimientoX, int movimientoY) {
        super(imagen, x, y);
        this.movimientoX = movimientoX;
        this.movimientoY = movimientoY;
        this.limiteDerecho = x + 51;
        this.limiteIzquierdo = x - 51;
        this.areaColisionProyectil = new Rectangle(8, 8, 16, 16);
    }

    @Override
    public void disparar() {

        x += movimientoX;

        if (x >= limiteDerecho || x <= limiteIzquierdo) {
            movimientoX = movimientoX * -1;
        }

        y -= movimientoY;

    }

    public void dispararLinea() {
        x += movimientoX;
        y += movimientoY;
    }
}
