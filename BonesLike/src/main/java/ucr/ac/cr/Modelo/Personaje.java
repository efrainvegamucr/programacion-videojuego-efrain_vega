/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ucr.ac.cr.Modelo;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import ucr.ac.cr.Vista.PanelJuego;

public class Personaje {

    private ImageIcon imagen;
    private int x;
    private int y;
    private int ataque;
    private int vida;
    private int movimiento;
    private boolean invulnerable;
    private long invulnerableTiempo;

    private boolean disparar;
    private long firingTimer;
    private long firingDeley;

    private Rectangle areaColisionCabeza;
    private Rectangle areaColisionCuerpo;

    private boolean arriba;
    private boolean abajo;
    private boolean izquierda;
    private boolean derecha;

    private ImageIcon imagenVida;

    private ImageIcon imagenArriba;
    private ImageIcon imagenAbajo;
    private ImageIcon imagenIzquierda;
    private ImageIcon imagenDerecha;

    private ImageIcon caminarArriba;
    private ImageIcon caminarAbajo;
    private ImageIcon caminarIzquierda;
    private ImageIcon caminarDerecha;

    private ArrayList<Proyectil_Personaje> disparos;
    private int direccion;

    private Sonido sonidoPersonajeDisparo;

    public Personaje(ImageIcon imagen, int x, int y) {

        this.imagen = imagen;
        this.x = x;
        this.y = y;
        this.ataque = 2;
        this.vida = 5;
        this.movimiento = 2;
        this.invulnerable = false;
        this.invulnerableTiempo = 0;
        this.disparar = false;

        this.firingTimer = System.nanoTime();
        this.firingDeley = 200;

        this.imagenVida = new ImageIcon("./src/main/resources/Imagenes.Personaje/Personaje_Vida.png");

        this.imagenArriba = new ImageIcon("./src/main/resources/Imagenes.Personaje/Personaje_Arriba.png");
        this.imagenAbajo = new ImageIcon("./src/main/resources/Imagenes.Personaje/Personaje_Abajo.png");
        this.imagenIzquierda = new ImageIcon("./src/main/resources/Imagenes.Personaje/Personaje_Izquierda.png");
        this.imagenDerecha = new ImageIcon("./src/main/resources/Imagenes.Personaje/Personaje_Derecha.png");

        this.caminarArriba = new ImageIcon("./src/main/resources/Imagenes.Personaje/Personaje_Arriba.gif");
        this.caminarAbajo = new ImageIcon("./src/main/resources/Imagenes.Personaje/Personaje_Abajo.gif");
        this.caminarIzquierda = new ImageIcon("./src/main/resources/Imagenes.Personaje/Personaje_Izquierda.gif");
        this.caminarDerecha = new ImageIcon("./src/main/resources/Imagenes.Personaje/Personaje_Derecha.gif");

        areaColisionCabeza = new Rectangle(2, 18, 60, 26);
        areaColisionCuerpo = new Rectangle(18, 6, 28, 90);

        disparos = new ArrayList<Proyectil_Personaje>();
    }

    public ImageIcon getImagen() {
        return imagen;
    }

    public void setImagen(ImageIcon imagen) {
        this.imagen = imagen;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getAtaque() {
        return ataque;
    }

    public void setAtaque(int ataque) {
        this.ataque = ataque;
    }

    public int getVida() {
        return vida;
    }

    public void setVida(int vida) {
        this.vida = vida;
    }

    public int getMovimiento() {
        return movimiento;
    }

    public void setMovimiento(int movimiento) {
        this.movimiento = movimiento;
    }

    public boolean getEstaInvulnerable() {
        return invulnerable;
    }

    public void setEstaInvulnerable(boolean invulnerable) {
        this.invulnerable = invulnerable;
    }

    public boolean isInvulnerable() {
        return invulnerable;
    }

    public void setInvulnerable(boolean invulnerable) {
        this.invulnerable = invulnerable;
    }

    public long getInvulnerableTiempo() {
        return invulnerableTiempo;
    }

    public void setInvulnerableTiempo(long invulnerableTiempo) {
        this.invulnerableTiempo = invulnerableTiempo;
    }

    public Rectangle getAreaColisionCabeza() {
        return areaColisionCabeza;
    }

    public void setAreaColisionCabeza(Rectangle areaColisionCabeza) {
        this.areaColisionCabeza = areaColisionCabeza;
    }

    public Rectangle getAreaColisionCuerpo() {
        return areaColisionCuerpo;
    }

    public void setAreaColisionCuerpo(Rectangle areaColisionCuerpo) {
        this.areaColisionCuerpo = areaColisionCuerpo;
    }

    public ArrayList<Proyectil_Personaje> getDisparos() {
        return disparos;
    }

    public void setDisparos(ArrayList<Proyectil_Personaje> disparos) {
        this.disparos = disparos;
    }

    public int getDireccion() {
        return direccion;
    }

    public void setDireccion(int direccion) {
        this.direccion = direccion;
    }

    public void setIzquierda(boolean estado) {
        izquierda = estado;
    }

    public void setDerecha(boolean estado) {
        derecha = estado;
    }

    public void setArriba(boolean estado) {
        arriba = estado;
    }

    public void setAbajo(boolean estado) {
        abajo = estado;
    }

    public void caminar() {
        if (arriba) {
            imagen = caminarArriba;
            y = y - movimiento;
            direccion = 0;
        }
        if (abajo) {
            imagen = caminarAbajo;
            y = y + movimiento;
            direccion = 1;
        }
        if (izquierda) {
            imagen = caminarIzquierda;
            x = x - movimiento;
            direccion = 2;
        }
        if (derecha) {
            imagen = caminarDerecha;
            x = x + movimiento;
            direccion = 3;
        }
        if (!arriba && !abajo && !izquierda && !derecha) {
            if (direccion == 0) {
                imagen = imagenArriba;

            } else if (direccion == 1) {
                imagen = imagenAbajo;

            } else if (direccion == 2) {
                imagen = imagenIzquierda;

            } else if (direccion == 3) {
                imagen = imagenDerecha;
            }
        }
    }

    public boolean getDisparar() {
        return disparar;
    }

    public void setDisparar(boolean disparar) {
        this.disparar = disparar;
    }

    public void disparar() {
        long elapsed = (System.nanoTime() - firingTimer) / 1000000;

        if (elapsed > firingDeley) {
            firingTimer = System.nanoTime();
            if (disparar) {
                sonidoPersonajeDisparo = new Sonido("./src/main/resources/Musica/PersonajeDisparo.wav");
                sonidoPersonajeDisparo.iniciar();
                disparos.add(new Proyectil_Personaje(new ImageIcon("./src/main/resources/Imagenes.Personaje/Proyectil.gif"), x + 16, y + 32, direccion));
            }
        }
    }

    public void tiempoInvulnerable() {
        long lapso = (System.nanoTime() - invulnerableTiempo) / 1000000;
        if (lapso > 2000) {
            invulnerable = false;
            invulnerableTiempo = 0;
        }
    }

    public void paintProyectiles(Graphics g, PanelJuego panelJuego) {
        for (int i = 0; i < disparos.size(); i++) {
            disparos.get(i).disparar();
            disparos.get(i).paint(g, panelJuego);
        }
    }

    public void paintVidas(Graphics g, PanelJuego panelJuego) {
        int inicio = 42;

        for (int i = 0; i < vida; i++) {
            imagenVida.paintIcon(panelJuego, g, inicio, 34);
            inicio += 56;
        }
    }

    public void paint(Graphics g, PanelJuego panelJuego) {
        imagen.paintIcon(panelJuego, g, x, y);
    }
}
