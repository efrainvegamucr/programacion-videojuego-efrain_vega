/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ucr.ac.cr.Modelo;

import java.awt.Rectangle;
import javax.swing.ImageIcon;

public class Enemigo_Vampiro extends Enemigo {

    private int disparoX;
    private int disparoY;
    private long disparoTimer;
    private long disparoLineaTimer;
    private long disparoDeley;
    private long disparoLineaDeley;
    private int randomPosicion;
    private int limiteDerecho;
    private int limiteIzquierdo;
    private long moveTimer;
    private long moveDeley;
    private Sonido vampiroDisparo;
    private Sonido vampiroDisparoCurva;
    private Sonido piano;
    private boolean mitadSonidoEnemigo;

    public Enemigo_Vampiro(ImageIcon imagen, int x, int y) {
        super(imagen, x, y);
        this.vidaMaxima = 64;
        this.vida = 64;
        this.areaColision = new Rectangle(x + 38, y + 19, 39, 93);
        this.movimiento = -4;
        this.limiteDerecho = 924;
        this.limiteIzquierdo = 324;
        this.disparoX = 2;
        this.disparoY = 3;
        this.disparoTimer = System.nanoTime();
        this.disparoLineaTimer = System.nanoTime();
        this.disparoDeley = 1800;
        this.disparoLineaDeley = 600;
        this.moveTimer = System.nanoTime();
        this.moveDeley = 100;
    }

    public void tiempoDisparo() {
        int posicion;
        long elapsedLinea = (System.nanoTime() - disparoTimer) / 1000000;
        long elapsedCurva = (System.nanoTime() - disparoLineaTimer) / 1000000;

        if (elapsedCurva > disparoLineaDeley) {
            vampiroDisparo = new Sonido("./src/main/resources/Musica/VampiroDisparo.wav");
            vampiroDisparo.iniciar();
            posicion = (int) ((Math.random() * (4 - 1)) + 1);
            this.randomPosicion = posicion;
            if (randomPosicion == 1) {
                this.disparosLinea.add(new Proyectil_Vampiro(new ImageIcon("./src/main/resources/Imagenes.Vampiro/Vampiro_Proyectil_Linea.png"), 624 + 38 * 16, 150 + 40 * 2, -(disparoX + 2), 0));
                this.disparosLinea.add(new Proyectil_Vampiro(new ImageIcon("./src/main/resources/Imagenes.Vampiro/Vampiro_Proyectil_Linea.png"), 624 - 38 * 16, 150 + 40 * 14, disparoX + 2, 0));
            }
            if (randomPosicion == 2) {
                this.disparosLinea.add(new Proyectil_Vampiro(new ImageIcon("./src/main/resources/Imagenes.Vampiro/Vampiro_Proyectil_Linea.png"), 624 - 38 * 16, 150 + 40 * 6, disparoX + 2, 0));
                this.disparosLinea.add(new Proyectil_Vampiro(new ImageIcon("./src/main/resources/Imagenes.Vampiro/Vampiro_Proyectil_Linea.png"), 624 + 38 * 16, 150 + 40 * 18, -(disparoX + 2), 0));
            }
            if (randomPosicion == 3) {
                this.disparosLinea.add(new Proyectil_Vampiro(new ImageIcon("./src/main/resources/Imagenes.Vampiro/Vampiro_Proyectil_Linea.png"), 624 + 38 * 16, 150 + 40 * 10, -(disparoX + 2), 0));
            }
            disparoLineaTimer = System.nanoTime();
        }

        if (vida <= vidaMaxima / 2) {
            disparoLineaDeley = 1500;
            if (elapsedLinea > disparoDeley) {
                vampiroDisparoCurva = new Sonido("./src/main/resources/Musica/VampiroDisparoCurva.wav");
                vampiroDisparoCurva.iniciar();
                this.disparos.add(new Proyectil_Vampiro(new ImageIcon("./src/main/resources/Imagenes.Vampiro/Vampiro_Proyectil.png"), 624 - 38 * 5, 192, disparoX, -disparoY));
                this.disparos.add(new Proyectil_Vampiro(new ImageIcon("./src/main/resources/Imagenes.Vampiro/Vampiro_Proyectil.png"), 624 - 38 * 10, 192, disparoX, -disparoY));
                this.disparos.add(new Proyectil_Vampiro(new ImageIcon("./src/main/resources/Imagenes.Vampiro/Vampiro_Proyectil.png"), 624 - 38 * 15, 192, disparoX, -disparoY));

                this.disparos.add(new Proyectil_Vampiro(new ImageIcon("./src/main/resources/Imagenes.Vampiro/Vampiro_Proyectil.png"), 624, 192, disparoX, -disparoY));

                this.disparos.add(new Proyectil_Vampiro(new ImageIcon("./src/main/resources/Imagenes.Vampiro/Vampiro_Proyectil.png"), 624 + 38 * 5, 192, disparoX, -disparoY));
                this.disparos.add(new Proyectil_Vampiro(new ImageIcon("./src/main/resources/Imagenes.Vampiro/Vampiro_Proyectil.png"), 624 + 38 * 10, 192, disparoX, -disparoY));
                this.disparos.add(new Proyectil_Vampiro(new ImageIcon("./src/main/resources/Imagenes.Vampiro/Vampiro_Proyectil.png"), 624 + 38 * 15, 192, disparoX, -disparoY));
                disparoTimer = System.nanoTime();
            }
        }
    }
    
    public void verificarMitadVida(double tiempo) {
        if (vida <= vidaMaxima / 2 && !mitadSonidoEnemigo) {
            piano =  new Sonido("./src/main/resources/Musica/Piano.wav");
            piano.setMicrosegundos(tiempo);
            piano.ciclo();
            mitadSonidoEnemigo = true;
        }
    }
    
    public Sonido getPiano() {
        return piano;
    }
    
    @Override
    public void movimiento(int x, int y) {
        long elapsed = (System.nanoTime() - moveTimer) / 1000000;
        if (elapsed > moveDeley) {
            this.x += movimiento;

            if (this.x >= limiteDerecho || this.x <= limiteIzquierdo) {
                movimiento = movimiento * -1;
            }

            this.areaColision = new Rectangle(this.x + 38, this.y + 19, 39, 93);
            this.imagen = new ImageIcon("./src/main/resources/Imagenes.Vampiro/Vampiro.gif");
            moveTimer = System.nanoTime();
        }
    }
    
    @Override
    public void movimientoDisparo() {
        for (int i = 0; i < disparos.size(); i++) {
            disparos.get(i).disparar();
        }
    }

    public void movimientoDisparoLinea() {
        for (int i = 0; i < disparosLinea.size(); i++) {
            disparosLinea.get(i).dispararLinea();
        }
    }

}
