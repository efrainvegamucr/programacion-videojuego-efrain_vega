/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ucr.ac.cr.Modelo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class RegistroPuntuacion {

    private ArrayList<Puntuacion> listaPuntuaciones;
    private File archivo;
    private JSONObject baseJSONPuntuacion;

    public RegistroPuntuacion() {
        listaPuntuaciones = new ArrayList<Puntuacion>();
        archivo = new File("puntuacion.json");

        if (archivo.exists()) {
            leerJSON();
        }
    }

    public void escribirJSON() {
        JSONArray arregloPuntuaciones = new JSONArray();
        baseJSONPuntuacion = new JSONObject();

        for (Puntuacion puntuacion : listaPuntuaciones) {
            JSONObject objJSONPuntuacion = new JSONObject();
            objJSONPuntuacion.put("nombre", puntuacion.getNombre());
            objJSONPuntuacion.put("puntuacion", puntuacion.getPuntuacion());
            arregloPuntuaciones.add(objJSONPuntuacion);
        }
        baseJSONPuntuacion.put("listaPuntuacion", arregloPuntuaciones);
        Collections.sort(listaPuntuaciones, Comparator.comparing(Puntuacion::getPuntuacion));

        try {
            FileWriter archivoEscribe = new FileWriter(archivo);
            archivoEscribe.write(baseJSONPuntuacion.toJSONString());
            archivoEscribe.flush();
            archivoEscribe.close();
        } catch (IOException ex) {
            System.err.println("Error al escribir en el archivo.");
        }
    }

    public void leerJSON() {

        JSONParser convierte = new JSONParser();
        try {
            FileReader archivoLee = new FileReader(archivo);
            Object obj = convierte.parse(archivoLee);
            baseJSONPuntuacion = (JSONObject) obj;

            JSONArray arregloJSON = (JSONArray) baseJSONPuntuacion.get("listaPuntuacion");
            for (Object object : arregloJSON) {
                JSONObject objPuntuacion = (JSONObject) object;
                Puntuacion puntuacion = new Puntuacion();
                puntuacion.setNombre(objPuntuacion.get("nombre").toString());
                puntuacion.setPuntuacion(objPuntuacion.get("puntuacion").toString());
                listaPuntuaciones.add(puntuacion);
                Collections.sort(listaPuntuaciones, Comparator.comparing(Puntuacion::getPuntuacion));
                Collections.reverse(listaPuntuaciones);
            }

        } catch (FileNotFoundException ex) {
            System.err.println("Archivo no encontrado");
        } catch (IOException ex) {
            System.err.println("Error al leer");
        } catch (org.json.simple.parser.ParseException ex) {
            ex.printStackTrace();
        }
    }

    public void reiniciarJSON() {
        JSONArray arregloPuntuaciones = new JSONArray();
        baseJSONPuntuacion = new JSONObject();
        baseJSONPuntuacion.put("listaPuntuacion", arregloPuntuaciones);
        try {
            FileWriter archivoEscribe = new FileWriter(archivo);
            archivoEscribe.write(baseJSONPuntuacion.toJSONString());
            archivoEscribe.flush();
            archivoEscribe.close();
        } catch (IOException ex) {
            System.err.println("Error al escribir en el archivo.");
        }
    }

    public String[] getPuntuaciones() {
        String[] listaPuntuacion = new String[listaPuntuaciones.size()];

        for (int indice = 0; indice < listaPuntuaciones.size(); indice++) {
            listaPuntuacion[indice] = listaPuntuaciones.get(indice).getPuntuacion();
        }
        return listaPuntuacion;
    }

    public String[][] getDatosTabla() {
        String[][] datos = new String[listaPuntuaciones.size()][Puntuacion.ETIQUETAS_PUNTUACION.length];
        for (int indice = 0; indice < listaPuntuaciones.size(); indice++) {
            for (int atributo = 0; atributo < datos[indice].length; atributo++) {
                datos[indice][atributo] = this.listaPuntuaciones.get(indice).setDatosPuntuacion(atributo);
            }
        }
        return datos;
    }

    public boolean Agregar(Puntuacion puntuacion) {
        boolean mensaje = false;
        if (listaPuntuaciones.add(puntuacion)) {
            escribirJSON();
            Collections.sort(listaPuntuaciones, Comparator.comparing(Puntuacion::getPuntuacion));
        } else {
            mensaje = true;
        }
        return mensaje;
    }

}
