/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ucr.ac.cr.Modelo;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import ucr.ac.cr.Vista.PanelJuego;

public class AreaJuego {

    private ImageIcon imagenMapa;
    private ImageIcon imagenCeldaCerrada;
    private ImageIcon imagenCeldaAbierta;
    private ImageIcon imagenVida;

    private Personaje personaje;
    private Enemigo enemigo;

    private Rectangle paredArribaIzquierda;
    private Rectangle paredArribaCentro;
    private Rectangle paredArribaDerecha;
    private Rectangle paredIzquierda;
    private Rectangle paredDerecha;
    private Rectangle paredAbajo;

    private Sonido personajeHit;

    private JLabel txtPuntuacion;
    private Font fuente = new Font("FFF Forward", 1, 32);

    public AreaJuego(ImageIcon imagenMapa) {
        this.imagenMapa = imagenMapa;
    }

    public AreaJuego(ImageIcon imagenMapa, Personaje personaje, int puntuacion) {
        this.imagenMapa = imagenMapa;
        this.personaje = personaje;

        this.paredArribaIzquierda = new Rectangle(448, 60, 147, 63);
        this.paredArribaCentro = new Rectangle(595, 64, 90, 59);
        this.paredArribaDerecha = new Rectangle(685, 60, 147, 63);
        this.paredIzquierda = new Rectangle(448, 0, 30, 768);
        this.paredDerecha = new Rectangle(802, 0, 30, 768);
        this.paredAbajo = new Rectangle(448, 738, 384, 30);
    }

    public AreaJuego(ImageIcon imagenMapa, Personaje personaje, Enemigo enemigo, int puntuacion) {
        this.imagenMapa = imagenMapa;
        this.imagenCeldaCerrada = new ImageIcon("./src/main/resources/Imagenes.Mapas/Mapa_Barrotes.png");
        this.imagenCeldaAbierta = new ImageIcon("./src/main/resources/Imagenes.Mapas/Mapa_Barrotes.gif");
        this.imagenVida = new ImageIcon("./src/main/resources/Imagenes.Mapas/Barra_Vida.png");
        this.personaje = personaje;
        this.enemigo = enemigo;

        this.paredArribaIzquierda = new Rectangle(30, 60, 568, 63);
        this.paredArribaCentro = new Rectangle(595, 64, 90, 59);
        this.paredArribaDerecha = new Rectangle(682, 60, 568, 63);
        this.paredIzquierda = new Rectangle(0, 128, 30, 640);
        this.paredDerecha = new Rectangle(1250, 128, 30, 640);
        this.paredAbajo = new Rectangle(0, 743, 1280, 25);
    }

    public void colisionPersonajeMapa() {
        personaje.setAreaColisionCabeza(new Rectangle(personaje.getX() + 2, personaje.getY() + 18, 60, 26));
        personaje.setAreaColisionCuerpo(new Rectangle(personaje.getX() + 18, personaje.getY() + 6, 28, 90));

        if (personaje.getAreaColisionCabeza().intersects(paredArribaIzquierda.getBounds())) {
            personaje.setY(personaje.getY() + personaje.getMovimiento());
        } else if (personaje.getAreaColisionCabeza().intersects(paredArribaCentro.getBounds())) {
            personaje.setY(personaje.getY() + personaje.getMovimiento());
        } else if (personaje.getAreaColisionCabeza().intersects(paredArribaDerecha.getBounds())) {
            personaje.setY(personaje.getY() + personaje.getMovimiento());
        } else if (personaje.getAreaColisionCabeza().intersects(paredIzquierda.getBounds())) {
            personaje.setX(personaje.getX() + personaje.getMovimiento());
        } else if (personaje.getAreaColisionCabeza().intersects(paredDerecha.getBounds())) {
            personaje.setX(personaje.getX() - personaje.getMovimiento());
        } else if (personaje.getAreaColisionCuerpo().intersects(paredAbajo.getBounds())) {
            personaje.setY(personaje.getY() - personaje.getMovimiento());
        }
    }

    public void colisionProyectilPersonajeMapa() {
        for (int i = 0; i < personaje.getDisparos().size(); i++) {

            personaje.getDisparos().get(i).setAreaColisionProyectil(new Rectangle(personaje.getDisparos().get(i).getX() + 16, personaje.getDisparos().get(i).getY() + 16, 32, 32));

            if (personaje.getDisparos().get(i).getAreaColisionProyectil().intersects(paredArribaIzquierda.getBounds())) {
                personaje.getDisparos().remove(i);
            } else if (personaje.getDisparos().get(i).getAreaColisionProyectil().intersects(paredArribaDerecha.getBounds())) {
                personaje.getDisparos().remove(i);
            } else if (personaje.getDisparos().get(i).getAreaColisionProyectil().intersects(paredArribaCentro.getBounds())) {
                personaje.getDisparos().remove(i);
            } else if (personaje.getDisparos().get(i).getAreaColisionProyectil().intersects(paredIzquierda.getBounds())) {
                personaje.getDisparos().remove(i);
            } else if (personaje.getDisparos().get(i).getAreaColisionProyectil().intersects(paredDerecha.getBounds())) {
                personaje.getDisparos().remove(i);
            } else if (personaje.getDisparos().get(i).getAreaColisionProyectil().intersects(paredAbajo.getBounds())) {
                personaje.getDisparos().remove(i);
            }
        }
    }

    public boolean colisionProyectilPersonajeEnemigo() {
        boolean colision = false;
        if (enemigo.getEstaVivo()) {
            for (int i = 0; i < personaje.getDisparos().size(); i++) {

                personaje.getDisparos().get(i).setAreaColisionProyectil(new Rectangle(personaje.getDisparos().get(i).getX() + 8, personaje.getDisparos().get(i).getY() + 8, 16, 16));

                if (personaje.getDisparos().get(i).getAreaColisionProyectil().intersects(enemigo.getAreaColision().getBounds())) {
                    personaje.getDisparos().remove(i);
                    golpearEnemigo();
                    colision = true;
                }
            }
        }
        return colision;
    }

    public void colisionEnemigoPersonaje() {
        if (!personaje.getEstaInvulnerable() && enemigo.getEstaVivo()) {
            if (personaje.getAreaColisionCabeza().intersects(enemigo.getAreaColision().getBounds()) || personaje.getAreaColisionCuerpo().intersects(enemigo.getAreaColision().getBounds())) {
                personajeHit = new Sonido("./src/main/resources/Musica/PersonajeHit.wav");
                personajeHit.iniciar();
                personaje.setVida(personaje.getVida() - 1);
                personaje.setEstaInvulnerable(true);
                personaje.setInvulnerableTiempo(System.nanoTime());
            }
        }
    }

    public void colisionProyectilEnemigoMapa() {
        for (int i = 0; i < enemigo.getDisparos().size(); i++) {

            enemigo.getDisparos().get(i).setAreaColisionProyectil(new Rectangle(enemigo.getDisparos().get(i).getX() + 8, enemigo.getDisparos().get(i).getY() + 8, 16, 16));

            if (enemigo.getDisparos().get(i).getAreaColisionProyectil().intersects(paredArribaIzquierda.getBounds())) {
                enemigo.getDisparos().remove(i);
            } else if (enemigo.getDisparos().get(i).getAreaColisionProyectil().intersects(paredArribaDerecha.getBounds())) {
                enemigo.getDisparos().remove(i);
            } else if (enemigo.getDisparos().get(i).getAreaColisionProyectil().intersects(paredArribaCentro.getBounds())) {
                enemigo.getDisparos().remove(i);
            } else if (enemigo.getDisparos().get(i).getAreaColisionProyectil().intersects(paredIzquierda.getBounds())) {
                enemigo.getDisparos().remove(i);
            } else if (enemigo.getDisparos().get(i).getAreaColisionProyectil().intersects(paredDerecha.getBounds())) {
                enemigo.getDisparos().remove(i);
            } else if (enemigo.getDisparos().get(i).getAreaColisionProyectil().intersects(paredAbajo.getBounds())) {
                enemigo.getDisparos().remove(i);
            }
        }
    }

    public void rebotarProyectilEnemigoMapa() {
        for (int i = 0; i < enemigo.getDisparos().size(); i++) {

            enemigo.getDisparos().get(i).setAreaColisionProyectil(new Rectangle(enemigo.getDisparos().get(i).getX() + 8, enemigo.getDisparos().get(i).getY() + 8, 16, 16));

            if (enemigo.getDisparos().get(i).getAreaColisionProyectil().intersects(paredArribaIzquierda.getBounds())) {
                enemigo.getDisparos().get(i).setMovimientoY(-enemigo.getDisparos().get(i).getMovimientoY());
            } else if (enemigo.getDisparos().get(i).getAreaColisionProyectil().intersects(paredArribaDerecha.getBounds())) {
                enemigo.getDisparos().get(i).setMovimientoY(-enemigo.getDisparos().get(i).getMovimientoY());
            } else if (enemigo.getDisparos().get(i).getAreaColisionProyectil().intersects(paredArribaCentro.getBounds())) {
                enemigo.getDisparos().get(i).setMovimientoY(-enemigo.getDisparos().get(i).getMovimientoY());
            } else if (enemigo.getDisparos().get(i).getAreaColisionProyectil().intersects(paredIzquierda.getBounds())) {
                enemigo.getDisparos().get(i).setMovimientoX(-enemigo.getDisparos().get(i).getMovimientoX());
            } else if (enemigo.getDisparos().get(i).getAreaColisionProyectil().intersects(paredDerecha.getBounds())) {
                enemigo.getDisparos().get(i).setMovimientoX(-enemigo.getDisparos().get(i).getMovimientoX());
            } else if (enemigo.getDisparos().get(i).getAreaColisionProyectil().intersects(paredAbajo.getBounds())) {
                enemigo.getDisparos().get(i).setMovimientoY(-enemigo.getDisparos().get(i).getMovimientoY());
            }
        }
    }

    public void colisionProyectilEnemigoPersonaje() {
        if (enemigo.getEstaVivo()) {
            for (int i = 0; i < enemigo.getDisparos().size(); i++) {

                enemigo.getDisparos().get(i).setAreaColisionProyectil(new Rectangle(enemigo.getDisparos().get(i).getX() + 8, enemigo.getDisparos().get(i).getY() + 8, 16, 16));

                if (enemigo.getDisparos().get(i).getAreaColisionProyectil().intersects(personaje.getAreaColisionCabeza().getBounds()) || enemigo.getDisparos().get(i).getAreaColisionProyectil().intersects(personaje.getAreaColisionCuerpo().getBounds())) {
                    enemigo.getDisparos().remove(i);
                    if (!personaje.getEstaInvulnerable()) {
                        personajeHit = new Sonido("./src/main/resources/Musica/PersonajeHit.wav");
                        personajeHit.iniciar();
                        personaje.setVida(personaje.getVida() - 1);
                        personaje.setEstaInvulnerable(true);
                        personaje.setInvulnerableTiempo(System.nanoTime());
                    }
                }
            }
        }
    }

    public void colisionProyectilEnemigoLineaPersonaje() {
        if (enemigo.getEstaVivo()) {
            for (int i = 0; i < enemigo.getDisparosLinea().size(); i++) {

                enemigo.getDisparosLinea().get(i).setAreaColisionProyectil(new Rectangle(enemigo.getDisparosLinea().get(i).getX() + 8, enemigo.getDisparosLinea().get(i).getY() + 8, 16, 16));

                if (enemigo.getDisparosLinea().get(i).getAreaColisionProyectil().intersects(personaje.getAreaColisionCabeza().getBounds()) || enemigo.getDisparosLinea().get(i).getAreaColisionProyectil().intersects(personaje.getAreaColisionCuerpo().getBounds())) {
                    enemigo.getDisparosLinea().remove(i);
                    if (!personaje.getEstaInvulnerable()) {
                        personajeHit = new Sonido("./src/main/resources/Musica/PersonajeHit.wav");
                        personajeHit.iniciar();
                        personaje.setVida(personaje.getVida() - 1);
                        personaje.setEstaInvulnerable(true);
                        personaje.setInvulnerableTiempo(System.nanoTime());
                    }
                }
            }
        }
    }

    public void colisionAreaEnemigoPersonaje() {
        if (enemigo.getEstaVivo()) {
            for (int i = 0; i < enemigo.getAreas().size(); i++) {

                enemigo.getAreas().get(i).setAreaColisionZona(new Rectangle(enemigo.getAreas().get(i).getX(), enemigo.getAreas().get(i).getY(), 48, 48));

                if (enemigo.getAreas().get(i).getAreaColisionZona().intersects(personaje.getAreaColisionCuerpo().getBounds())) {
                    if (!personaje.getEstaInvulnerable()) {
                        personajeHit = new Sonido("./src/main/resources/Musica/PersonajeHit.wav");
                        personajeHit.iniciar();
                        personaje.setVida(personaje.getVida() - 1);
                        personaje.setEstaInvulnerable(true);
                        personaje.setInvulnerableTiempo(System.nanoTime());
                    }
                }
            }
        }
    }

    public void golpearEnemigo() {
        enemigo.setVida(enemigo.getVida() - personaje.getAtaque());

        if (enemigo.getVida() <= 0) {
            imagenCeldaCerrada = imagenCeldaAbierta;
            enemigo.setEstaVivo(false);
        }
    }

    public boolean siguienteNivel() {
        boolean colision = false;

        if (personaje.getAreaColisionCabeza().intersects(paredArribaCentro.getBounds())) {
            colision = true;
            personaje.setX(610);
            personaje.setY(628);
            personaje.setDisparos(new ArrayList<Proyectil_Personaje>());

            if (enemigo != null) {
                imagenCeldaAbierta.getImage().flush();
            }
        }
        return colision;
    }

    public ImageIcon getImagenMapa() {
        return imagenMapa;
    }

    public void paintInicio(Graphics g, PanelJuego panelJuego) {
        imagenMapa.paintIcon(panelJuego, g, 0, 0);
        personaje.paintProyectiles(g, panelJuego);
        personaje.paintVidas(g, panelJuego);
        personaje.paint(g, panelJuego);
    }

    public void paintEnemigo(Graphics g, PanelJuego panel, int puntuacion) {
        double seccion = (double) 512 / enemigo.getVidaMaxima();
        double valorBarraVida = seccion * enemigo.getVida();

        imagenMapa.paintIcon(panel, g, 0, 0);
        imagenCeldaCerrada.paintIcon(panel, g, 0, 0);

        if (enemigo.getEstaVivo()) {
            g.setColor(Color.RED);
            g.fillRect(384, 746, (int) valorBarraVida, 14);
            imagenVida.paintIcon(panel, g, 372, 742);
        }

        enemigo.paintAreas(g, panel);
        enemigo.paint(g, panel);
        enemigo.paintProyectiles(g, panel);
        enemigo.paintProyectilesLinea(g, panel);
        personaje.paint(g, panel);
        personaje.paintProyectiles(g, panel);
        personaje.paintVidas(g, panel);

        JLabel txtPuntuacion = panel.getTxtPuntuacion();
        txtPuntuacion.reshape(0, 100, 500, 250);
        txtPuntuacion.setText("   Puntuacion: " + String.valueOf(puntuacion));
        txtPuntuacion.setForeground(Color.WHITE);
        txtPuntuacion.paint(g);
    }

    public void paintCreditos(Graphics g, PanelJuego panel) {
        imagenMapa.paintIcon(panel, g, 0, 0);
    }
}
