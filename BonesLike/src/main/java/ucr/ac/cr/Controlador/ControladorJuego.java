/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ucr.ac.cr.Controlador;

import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.ImageIcon;
import ucr.ac.cr.Modelo.AreaJuego;
import ucr.ac.cr.Modelo.Enemigo_Bruja;
import ucr.ac.cr.Modelo.Enemigo_Slime;
import ucr.ac.cr.Modelo.Enemigo_Vampiro;
import ucr.ac.cr.Modelo.Personaje;
import ucr.ac.cr.Modelo.Proyectil;
import ucr.ac.cr.Modelo.Sonido;
import ucr.ac.cr.Vista.GUIGameOver;
import ucr.ac.cr.Vista.GUIJuego;
import ucr.ac.cr.Vista.PanelJuego;

public class ControladorJuego implements KeyListener, Runnable {

    public int numero = 0;
    private int FPS = 60;
    private boolean finJuego;
    private int puntuacion;

    private Personaje personaje;
    private Enemigo_Slime slime;
    private Enemigo_Bruja bruja;
    private Enemigo_Vampiro vampiro;

    private ImageIcon slimeMuerte = new ImageIcon("./src/main/resources/Imagenes.Slime/Slime_Muerte.gif");
    private ImageIcon vampiroMuerte = new ImageIcon("./src/main/resources/Imagenes.Vampiro/Vampiro_Muerte.gif");
    private ImageIcon brujaMuerte = new ImageIcon("./src/main/resources/Imagenes.Bruja/Bruja_Muerte.gif");

    private AreaJuego areaInicio;
    private AreaJuego areaSlime;
    private AreaJuego areaBruja;
    private AreaJuego areaVampiro;
    private AreaJuego areaCreditos;

    private GUIJuego guiJuego;
    private PanelJuego panelJuego;

    private Timer timer;
    private TimerTask task;
    private ImageIcon mapa_Creditos = new ImageIcon("./src/main/resources/Imagenes.Mapas/Mapa_Creditos.gif");

    private boolean muerteSonidoSlime;
    private boolean muerteSonidoVampiro;
    private boolean muerteSonidoBruja;

    private Sonido sonidoPersonajeMuerte;
    private Sonido sonidoSlimeMuerte;
    private Sonido sonidoVampiroMuerte;
    private Sonido sonidoBrujaMuerte;

    private Thread hiloJuego = new Thread(this);

    public ControladorJuego() throws IOException {
        this.finJuego = false;
        this.puntuacion = 0;
        this.personaje = new Personaje(new ImageIcon("./src/main/resources/Imagenes.Personaje/Personaje_Arriba.png"), 611, 628);
        this.slime = new Enemigo_Slime(new ImageIcon("./src/main/resources/Imagenes.Slime/Slime.png"), 576, 228);
        this.bruja = new Enemigo_Bruja(new ImageIcon("./src/main/resources/Imagenes.Bruja/Bruja.gif"), 586, 168);
        this.vampiro = new Enemigo_Vampiro(new ImageIcon("./src/main/resources/Imagenes.Vampiro/Vampiro.gif"), 586, 168);

        this.slimeMuerte.getImage().flush();
        this.vampiroMuerte.getImage().flush();
        this.brujaMuerte.getImage().flush();

        this.muerteSonidoSlime = false;
        this.muerteSonidoVampiro = false;
        this.muerteSonidoBruja = false;

        this.areaInicio = new AreaJuego(new ImageIcon("./src/main/resources/Imagenes.Mapas/Mapa_Inicio.png"), personaje, puntuacion);
        this.areaSlime = new AreaJuego(new ImageIcon("./src/main/resources/Imagenes.Mapas/Mapa_Slime.png"), personaje, slime, puntuacion);
        this.areaBruja = new AreaJuego(new ImageIcon("./src/main/resources/Imagenes.Mapas/Mapa_Bruja.png"), personaje, bruja, puntuacion);
        this.areaVampiro = new AreaJuego(new ImageIcon("./src/main/resources/Imagenes.Mapas/Mapa_Vampiro.png"), personaje, vampiro, puntuacion);
        this.areaCreditos = new AreaJuego(mapa_Creditos);

        this.mapa_Creditos.getImage().flush();
        this.timer = new Timer();
    }

    public void setGUIJuego(GUIJuego guiJuego) {
        this.guiJuego = guiJuego;
    }

    public void setPanelJuego(PanelJuego panelJuego) {
        this.panelJuego = panelJuego;
    }

    public void iniciarHilo() {
        this.hiloJuego.start();
    }

    @Override
    public void run() {
        long startTime;
        long URDTimeMillis;
        long waitTime;

        long targetTime = 1000 / FPS;

        while (finJuego != true) {
            startTime = System.nanoTime();

            verificarPersonaje();
            verificarEnemigo();
            verificarColision();

            panelJuego.repaint();

            URDTimeMillis = ((System.nanoTime() - startTime) / 1000000);
            waitTime = targetTime - URDTimeMillis;
            try {
                Thread.sleep(waitTime);
            } catch (Exception e) {
            }
        }
    }

    public void verificarColision() {
        if (numero == 0) {
            areaInicio.colisionPersonajeMapa();
            areaInicio.colisionProyectilPersonajeMapa();
            if (areaInicio.siguienteNivel()) {
                numero++;
            }

        } else if (numero == 1) {
            areaSlime.colisionPersonajeMapa();
            areaSlime.colisionProyectilPersonajeMapa();
            if (areaSlime.colisionProyectilPersonajeEnemigo()) {
                puntuacion += 10;
            }
            areaSlime.colisionEnemigoPersonaje();
            areaSlime.rebotarProyectilEnemigoMapa();
            areaSlime.colisionProyectilEnemigoPersonaje();

            if (!slime.getEstaVivo() && areaSlime.siguienteNivel()) {
                numero++;
            }

        } else if (numero == 2) {
            areaVampiro.colisionPersonajeMapa();
            areaVampiro.colisionProyectilPersonajeMapa();
            if (areaVampiro.colisionProyectilPersonajeEnemigo()) {
                puntuacion += 10;
            }

            areaVampiro.colisionEnemigoPersonaje();
            areaVampiro.colisionProyectilEnemigoMapa();
            areaVampiro.colisionProyectilEnemigoPersonaje();
            areaVampiro.colisionProyectilEnemigoLineaPersonaje();
            if (!vampiro.getEstaVivo() && areaVampiro.siguienteNivel()) {
                numero++;
            }

        } else if (numero == 3) {
            areaBruja.colisionPersonajeMapa();
            areaBruja.colisionProyectilPersonajeMapa();
            if (areaBruja.colisionProyectilPersonajeEnemigo()) {
                puntuacion += 10;
            }

            areaBruja.colisionEnemigoPersonaje();
            areaBruja.colisionProyectilEnemigoMapa();
            areaBruja.colisionProyectilEnemigoPersonaje();
            areaBruja.colisionAreaEnemigoPersonaje();

            if (!bruja.getEstaVivo() && areaBruja.siguienteNivel()) {
                numero++;
            }
        }
    }

    public void verificarPersonaje() {
        personaje.caminar();
        personaje.disparar();
        personaje.tiempoInvulnerable();

        if (personaje.getVida() <= 0) {
            sonidoPersonajeMuerte = new Sonido("./src/main/resources/Musica/PersonajeMuerte.wav");
            sonidoPersonajeMuerte.iniciar();
            detenerJuego();
        }
    }

    public void verificarEnemigo() {
        if (numero == 1) {
            if (slime.getEstaVivo()) {
                slime.tiempoDisparo();
                slime.movimiento(personaje.getX(), personaje.getY());
                slime.movimientoDisparo();
                slime.verificarMitadVida(guiJuego.getViolin().getMicrosegundos());
            } else {
                if (!muerteSonidoSlime) {
                    slime.getImagen().getImage().flush();
                    sonidoSlimeMuerte = new Sonido("./src/main/resources/Musica/SlimeMuerte.wav");
                    sonidoSlimeMuerte.iniciar();
                    muerteSonidoSlime = true;
                    slime.getPiano().cerrar();
                }
                slime.setImagen(slimeMuerte);
                slime.setDisparos(new ArrayList<Proyectil>());
                slime.setDisparosLinea(new ArrayList<Proyectil>());
                slime.setAreas(new ArrayList<Proyectil>());
            }

        } else if (numero == 2) {
            if (vampiro.getEstaVivo()) {
                vampiro.tiempoDisparo();
                vampiro.movimiento(personaje.getX(), personaje.getY());
                vampiro.movimientoDisparo();
                vampiro.movimientoDisparoLinea();
                vampiro.verificarMitadVida(guiJuego.getViolin().getMicrosegundos());
            } else {
                if (!muerteSonidoVampiro) {
                    vampiro.getImagen().getImage().flush();
                    sonidoVampiroMuerte = new Sonido("./src/main/resources/Musica/VampiroMuerte.wav");
                    sonidoVampiroMuerte.iniciar();
                    muerteSonidoVampiro = true;
                    vampiro.getPiano().cerrar();
                }
                vampiro.setImagen(vampiroMuerte);
                vampiro.setDisparos(new ArrayList<Proyectil>());
                vampiro.setDisparosLinea(new ArrayList<Proyectil>());
                vampiro.setAreas(new ArrayList<Proyectil>());
            }

        } else if (numero == 3) {
            if (bruja.getEstaVivo()) {
                bruja.tiempoDisparo();
                bruja.tiempoZona();
                bruja.movimiento(personaje.getX(), personaje.getY());
                bruja.movimientoDisparo();
                bruja.verificarMitadVida(guiJuego.getViolin().getMicrosegundos());
            } else {
                if (!muerteSonidoBruja) {
                    bruja.getImagen().getImage().flush();
                    sonidoBrujaMuerte = new Sonido("./src/main/resources/Musica/BrujaMuerte.wav");
                    sonidoBrujaMuerte.iniciar();
                    muerteSonidoBruja = true;
                    bruja.getPiano().cerrar();
                }
                bruja.setImagen(brujaMuerte);
                bruja.setDisparos(new ArrayList<Proyectil>());
                bruja.setDisparosLinea(new ArrayList<Proyectil>());
                bruja.setAreas(new ArrayList<Proyectil>());
            }
        }
    }

    public void paint(Graphics g) {
        if (numero == 0) {
            areaInicio.paintInicio(g, panelJuego);
        } else if (numero == 1) {
            areaSlime.paintEnemigo(g, panelJuego, puntuacion);
        } else if (numero == 2) {
            areaVampiro.paintEnemigo(g, panelJuego, puntuacion);
        } else if (numero == 3) {
            areaBruja.paintEnemigo(g, panelJuego, puntuacion);
        } else if (numero == 4) {
            areaCreditos.paintCreditos(g, panelJuego);
            try {
                this.task = new TimerTask() {
                    @Override
                    public void run() {
                        detenerJuego();
                    }
                };
                timer.schedule(task, 17990, 1000);
            } catch (Exception e) {
            }
        }
    }

    public void detenerJuego() {
        double tiempo = guiJuego.getViolin().getMicrosegundos();
        try {
            if (numero == 1) {
                slime.getPiano().cerrar();
            } else if (numero == 2) {
                vampiro.getPiano().cerrar();
            } else if (numero == 3) {
                bruja.getPiano().cerrar();
            }
        } catch (Exception e) {
        }

        GUIGameOver guiGameOver = new GUIGameOver(puntuacion, tiempo);
        guiGameOver.show();

        guiJuego.dispose();
        guiJuego.getBateria().cerrar();
        guiJuego.getViolin().cerrar();

        timer.cancel();
        hiloJuego.stop();
    }

    @Override
    public void keyTyped(KeyEvent evento) {
    }

    @Override
    public void keyPressed(KeyEvent evento) {
        int keyCode = evento.getKeyCode();

        if (keyCode == KeyEvent.VK_UP) {
            personaje.setArriba(true);
        } else if (keyCode == KeyEvent.VK_DOWN) {
            personaje.setAbajo(true);
        } else if (keyCode == KeyEvent.VK_LEFT) {
            personaje.setIzquierda(true);
        } else if (keyCode == KeyEvent.VK_RIGHT) {
            personaje.setDerecha(true);
        } else if (keyCode == KeyEvent.VK_Z) {
            personaje.setDisparar(true);
        }
    }

    @Override
    public void keyReleased(KeyEvent evento) {
        int keyCode = evento.getKeyCode();

        if (keyCode == KeyEvent.VK_UP) {
            personaje.setArriba(false);
        } else if (keyCode == KeyEvent.VK_DOWN) {
            personaje.setAbajo(false);
        } else if (keyCode == KeyEvent.VK_LEFT) {
            personaje.setIzquierda(false);
        } else if (keyCode == KeyEvent.VK_RIGHT) {
            personaje.setDerecha(false);
        } else if (keyCode == KeyEvent.VK_Z) {
            personaje.setDisparar(false);
        }
    }
}
