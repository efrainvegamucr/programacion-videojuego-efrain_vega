/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ucr.ac.cr.Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import ucr.ac.cr.Vista.GUIEstadisticas;
import ucr.ac.cr.Vista.GUIInstrucciones;
import ucr.ac.cr.Vista.GUIJuego;
import ucr.ac.cr.Vista.GUIPrincipal;

public class ControladorPrincipal implements ActionListener {

    private GUIPrincipal guiPrincipal;
    private GUIJuego guiJuego;
    private GUIInstrucciones guiInstrucciones;
    private GUIEstadisticas guiEstadisticas;

    public ControladorPrincipal(GUIPrincipal guiPrincipal) {
        this.guiPrincipal = guiPrincipal;
    }

    @Override
    public void actionPerformed(ActionEvent evento) {
        String valorLeido = evento.getActionCommand();

        switch (valorLeido) {
            case "Jugar":
                guiPrincipal.dispose();
                guiPrincipal.getViolin().cerrar();
                try {
                    guiJuego = new GUIJuego(guiPrincipal.getViolin().getMicrosegundos());
                } catch (IOException ex) {
                    Logger.getLogger(ControladorPrincipal.class.getName()).log(Level.SEVERE, null, ex);
                }
                guiJuego.getControladorJuego().iniciarHilo();
                guiJuego.setVisible(true);
                break;

            case "Instrucciones":
                guiInstrucciones = new GUIInstrucciones(guiPrincipal.getControladorPrincipal());
                guiInstrucciones.setVisible(true);
                guiPrincipal.setVisible(false);
                break;
            case "Estadisticas":
                guiEstadisticas = new GUIEstadisticas(guiPrincipal.getControladorPrincipal());
                guiEstadisticas.setVisible(true);
                guiPrincipal.setVisible(false);
                break;
            case "Atras":
                try {
                guiEstadisticas.dispose();
                } 
                catch (Exception e) {
                }
                try {
                    guiInstrucciones.dispose();
                } 
                catch (Exception e) {
                }
            guiPrincipal.setVisible(true);
            break;
            case "Salir":
                guiPrincipal.dispose();
                break;
        }
    }
}
