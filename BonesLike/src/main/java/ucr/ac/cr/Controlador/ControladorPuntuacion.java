/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ucr.ac.cr.Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import ucr.ac.cr.Modelo.Puntuacion;
import ucr.ac.cr.Modelo.RegistroPuntuacion;
import ucr.ac.cr.Vista.GUIEstadisticas;
import ucr.ac.cr.Vista.GUIGameOver;
import ucr.ac.cr.Vista.GUIPrincipal;
import ucr.ac.cr.Vista.PanelEstadisticas;
import ucr.ac.cr.Vista.PanelGameOver;

/**
 *
 * @author efrai
 */
public class ControladorPuntuacion implements ActionListener {

    private Puntuacion puntuacion;
    private RegistroPuntuacion registroPuntuacion;

    private GUIGameOver guiGameOver;
    private PanelGameOver panelGameOver;
    private GUIEstadisticas guiEstadisticas;
    private PanelEstadisticas panelEstadisticas;
    private int cantidadPuntos = 0;

    public ControladorPuntuacion(GUIGameOver guiGameOver, PanelGameOver panelGameOver, int puntuacion) {
        registroPuntuacion = new RegistroPuntuacion();
        this.guiGameOver = guiGameOver;
        this.panelGameOver = panelGameOver;
        panelGameOver.setDatosTabla(registroPuntuacion.getDatosTabla());
        this.cantidadPuntos = puntuacion;
    }

    public ControladorPuntuacion(GUIEstadisticas guiEstadisticas, PanelEstadisticas panelEstadisticas) {
        registroPuntuacion = new RegistroPuntuacion();
        this.guiEstadisticas = guiEstadisticas;
        this.panelEstadisticas = panelEstadisticas;
        panelEstadisticas.setDatosTabla(registroPuntuacion.getDatosTabla());
    }

    public RegistroPuntuacion getRegistroPuntuacion() {
        return registroPuntuacion;
    }

    @Override
    public void actionPerformed(ActionEvent evento) {
        String valorLeido = evento.getActionCommand();
       

        switch (valorLeido) {
            case "Aceptar":
                double tiempo = guiGameOver.getViolin().getMicrosegundos();
                if (panelGameOver.getjTFNombre().equals("")) {
                    guiGameOver.mostrarMensaje("El campo Nombre debe contener informacion.");
                } else {
                    puntuacion = new Puntuacion(panelGameOver.getjTFNombre(), String.valueOf(cantidadPuntos));
                    if (registroPuntuacion.Agregar(puntuacion)) {
                        guiGameOver.mostrarMensaje("Por favor, registrar un nombre.");
                    }
                    panelGameOver.setDatosTabla(this.registroPuntuacion.getDatosTabla());

                    GUIPrincipal guiPrincipal = new GUIPrincipal(tiempo);
                    guiPrincipal.show();

                    guiGameOver.dispose();
                    guiGameOver.getViolin().cerrar();
                    break;
                }
            case "Reiniciar":
                registroPuntuacion.reiniciarJSON();
                panelEstadisticas.setDatosTabla(null);

        }
    }
}
